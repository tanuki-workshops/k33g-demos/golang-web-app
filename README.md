# golang-web-app

## Create a new Go project

```bash
mkdir hello-world
cd hello-world
go mod init hello-world
touch main.go
```

## Create a `go.work` file

Create a `go.work` file at the root of the project with this content:

```golang
go 1.20

use (
	./hello-world
)
```

## Build a static application
> with no dependency

```bash
#!/bin/bash
env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
go build -ldflags="-s -w" -o hello-world
```

## Dockerize the appplication

```Dockerfile
FROM scratch
COPY hello-world .
EXPOSE 8080
CMD ["/hello-world"]
```

## Deploy on Fly.io
> https://fly.io
You need to:

- Create a `fly.toml` configuration file
- Create an account on fly.io
- Create a fly.io token
  - Use https://fly.io/user/personal_access_tokens
  - Copy the generated value
  - Create an environment variable `FLY_ACCESS_TOKEN` (with the token value) at the group level or project level 


## Frameworks

- https://gofiber.io/
